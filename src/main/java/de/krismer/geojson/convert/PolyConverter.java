package de.krismer.geojson.convert;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.geojson.Geometry;
import org.geojson.LngLatAlt;
import org.geojson.MultiPolygon;
import org.geojson.Polygon;

public class PolyConverter {
	private static final String DEFAULT_EXTENSION_GEOJSON = "json";
	private static final Logger LOGGER = LogManager.getLogger(PolyConverter.class);
	private static final ObjectMapper MAPPER;
	private static final Charset RESULT_FILE_ENCODING = StandardCharsets.UTF_8;
	private String city;
	private String inputFile;
	private String outputDir;
	private Geometry<?> poly;

	static {
		MAPPER = new ObjectMapper()
			.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
			.enable(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY);
	}

	// Constructor

	public PolyConverter(final MultiPolygon mp, final String cityName, final String outputDir) {
		this.city = cityName;
		this.inputFile = null;
		this.outputDir = outputDir;
		this.poly = mp;

		initOutputDirectory();
	}

	public PolyConverter(final String inputFile, final String outputDir) {
		this.inputFile = inputFile;
		this.outputDir = outputDir;

		initFile();
		initOutputDirectory();
	}

	// Public methods

	public void writePoly() throws IOException {
		if (poly == null && inputFile == null) {
			throw new IllegalStateException("No input file (or polygon) set!");
		}

		if (poly == null) {
			poly = readGeoJson();
		}

		writePolyToOutputDir();
	}

	// Protected methods

	protected Geometry<?> readGeoJson() throws JsonParseException, JsonMappingException, IOException {
		if (inputFile == null) {
			throw new IllegalStateException("No input file found!");
		}
		final File f = new File(inputFile);
		if (!f.exists()) {
			throw new IllegalArgumentException("File \"" + inputFile + "\" does not exist!");
		}

		return MAPPER.readValue(f, Geometry.class);
	}

	protected void writePolyToOutputDir() throws IOException {
		if (poly == null) {
			throw new IllegalStateException("No input set!");
		}
		if (city == null) {
			throw new IllegalStateException("No city name parsed yet!");
		}

		final List<List<List<LngLatAlt>>> polygons;
		if (poly instanceof MultiPolygon) {
			polygons = ((MultiPolygon) poly).getCoordinates();
		} else if (poly instanceof Polygon) {
			polygons = new ArrayList<>(1);
			polygons.add(((Polygon) poly).getCoordinates());
		} else {
			throw new IllegalStateException("Polygon not a (Multi)Polygon!");
		}

		final String format = "%8.7E";
		final File f = new File(outputDir + File.separator + city + ".poly");
		try (BufferedWriter w = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(f, false), RESULT_FILE_ENCODING))) {
			w.write(city + "\n");
			int polygonIndex = 0;
			for (final List<List<LngLatAlt>> polygon : polygons) {
				int ringIndex = 0;
				for (final List<LngLatAlt> ring : polygon) {
					if (++ringIndex > 1) {
						// interior rings are subtracted (from exterior ring.. exterior ring is first in list ... all others are interior rings)
						w.write("!");
					}
					w.write(++polygonIndex + "\n");
					for (final LngLatAlt c : ring) {
						w.write("\t" + String.format(Locale.ENGLISH, format, c.getLongitude()));
						w.write("\t" + String.format(Locale.ENGLISH, format, c.getLatitude()));
						w.write("\n");
					}
					w.write("END\n");
				}
			}
			w.write("END\n");
			w.flush();
		}
	}

	// Private methods

	private void initFile() {
		final char fileExtensionSeparator = '.';
		final int i = inputFile.lastIndexOf(fileExtensionSeparator);
		if (i < 0) {
			city = inputFile;
			inputFile += fileExtensionSeparator + DEFAULT_EXTENSION_GEOJSON;
		} else {
			city = inputFile.substring(0, i);
		}

		final int j = city.lastIndexOf(File.separatorChar);
		if (j >= 0) {
			city = city.substring(j + 1);
		}
	}

	private void initOutputDirectory() {
		final File f = new File(outputDir);

		if (!f.exists()) {
			// output directory not existing -> try to create it (maybe it has been deleted since last call to initOutputDir)
			final boolean existsOuptutDir = f.exists();
			if (existsOuptutDir && !f.isDirectory()) {
				throw new IllegalArgumentException("Output directory object does not reference a directory");
			}

			if (!existsOuptutDir) {
				if (!f.mkdirs()) {
					LOGGER.warn("Could not create output directory!");
					throw new IllegalArgumentException("Could not create output directory!");
				}
			}
		}
	}

}
