package de.krismer.geojson;

import java.io.File;
import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import de.krismer.geojson.convert.PolyConverter;

public class Geojson2poly {
	private static final String DEFAULT_RESULT_DIR = "build" + File.separatorChar + "poly";
	private static final Logger LOGGER = LogManager.getLogger(Geojson2poly.class);

	@Option(name = "-f", aliases = "file", required = true, usage = "File that is converted to a poly file")
	private String inputFile;

	@Option(name = "-o", aliases = "outputDir", required = false, usage = "Output directory (where files will be written to)")
	private String resultDir = DEFAULT_RESULT_DIR;

	public static void main(final String[] args) throws IOException {
		LOGGER.info("Generating polygon from geojson");

		final Geojson2poly application = new Geojson2poly();
		final CmdLineParser parser = new CmdLineParser(application);
		try {
			parser.parseArgument(args);
		} catch (final CmdLineException | NumberFormatException e) {
			System.err.println("Invalid usage:" + e.getMessage());
			parser.printUsage(System.out);
			System.exit(1);
		}

		final PolyConverter converter = new PolyConverter(application.getInputFile(), application.getResultDir());
		converter.writePoly();
	}

	// Getters

	public String getInputFile() {
		return inputFile;
	}

	public String getResultDir() {
		return resultDir;
	}

}
