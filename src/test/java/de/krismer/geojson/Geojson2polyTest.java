package de.krismer.geojson;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class Geojson2polyTest {
	private static final String DEFAULT_RESULT_DIR = "build" + File.separatorChar + "poly";

	// Public methods

	@DataProvider
	public String[] getTestMultipolygons() {
		return new String[] { "bolzano", "italy" };
	}

	@DataProvider
	public String[] getTestPolygons() {
		return new String[] { "netherlands" };
	}

	@BeforeMethod
	@AfterMethod
	public void setup() throws IOException {
		final File f = new File(DEFAULT_RESULT_DIR);
		if (!f.exists()) {
			return;
		}
		if (!f.isDirectory()) {
			throw new IllegalStateException("Invalid output directory specified!");
		}

		FileUtils.deleteDirectory(f);
	}

	// Test methods

	@Test(dataProvider = "getTestMultipolygons")
	public void convertMultipolygon(final String city) throws IOException {
		testConversion(city);
	}

	@Test(dataProvider = "getTestPolygons")
	public void convertPolygon(final String city) throws IOException {
		testConversion(city);
	}

	// Private methods

	private void testConversion(final String city) throws IOException {
		final String[] opts = {
			"-f", "./src/test/resources/" + city + ".json",
			"-o", DEFAULT_RESULT_DIR
		};

		Assert.assertFalse(new File(DEFAULT_RESULT_DIR).exists());
		Geojson2poly.main(opts);
		Assert.assertTrue(new File(DEFAULT_RESULT_DIR).exists());
		Assert.assertTrue(new File(DEFAULT_RESULT_DIR + File.separatorChar + city + ".poly").exists());
	}

}

