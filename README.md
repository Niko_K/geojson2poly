geojson2poly
============

Conversion from geojson MultiPolygon objects to poly files (used by osmosis, ...).

Executing
---------
It is possible to convert a geojson file directly into a poly file by calling:

	./gradlew geojson2poly -Pfile=source.geojson


Compiling
---------
To get an executable jar use:

	./gradlew shadowJar


Usage
-----
Call

	./gradlew run

to get some usage information.


License
-------
geojson2poly is licensed under the GNU General Public License v3.
Please refer to LICENSE.md for more information.


Copyright
---------

(c) 2018 Nikolaus Krismer <niko@krismer.de>
